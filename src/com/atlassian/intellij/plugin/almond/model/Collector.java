package com.atlassian.intellij.plugin.almond.model;

import com.google.common.base.Predicate;
import com.google.common.collect.Sets;

import java.util.Collection;
import java.util.Set;

public class Collector
{

    public static <T> Set<T> consume(Collection<T> collection, final Predicate<T> predicate, final int size)
    {
        final Set<T> result = Sets.newHashSet();
        for (T t : collection)
        {
            if (result.size() == size) break;
            if (predicate.apply(t))
            {
                result.add(t);
            }
        }

        return result;
    }
}
