package com.atlassian.intellij.plugin.almond.model;

import com.intellij.openapi.vfs.VirtualFile;

public class JSIndexSearchResult
{
    private final VirtualFile virtualFile;
    private final String key;

    public JSIndexSearchResult(VirtualFile file, String key)
    {
        this.virtualFile = file;
        this.key = key;
    }

    public VirtualFile getVirtualFile()
    {
        return virtualFile;
    }

    public String getKey()
    {
        return key;
    }
}
