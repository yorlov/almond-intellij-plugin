package com.atlassian.intellij.plugin.almond.reference;

import com.atlassian.intellij.plugin.almond.model.JSIndexSearchResult;
import com.atlassian.intellij.plugin.almond.utils.ElementPatterns;
import com.atlassian.intellij.plugin.almond.utils.JSIndexSearcher;
import com.google.common.collect.Lists;
import com.intellij.openapi.util.text.StringUtil;
import com.intellij.psi.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;
import java.util.Set;

public class JSRequireReference extends PsiReferenceBase<PsiElement> implements PsiPolyVariantReference
{
    private final String keyword;
    private final List<PsiElement> elements;

    public JSRequireReference(@NotNull PsiElement element)
    {
        super(element);
        keyword = StringUtil.unquoteString(element.getText());
        Set<JSIndexSearchResult> files = JSIndexSearcher.get(keyword, element.getProject());
        elements = ElementPatterns.getDefines(files, element.getProject());
    }

    public boolean canResolve()
    {
        return !elements.isEmpty();
    }

    @NotNull
    @Override
    public ResolveResult[] multiResolve(boolean b)
    {
        List<ResolveResult> results = Lists.newArrayList();
        for (PsiElement element : elements)
        {
            results.add(new PsiElementResolveResult(element));
        }

        return results.toArray(new ResolveResult[results.size()]);
    }

    @Nullable
    @Override
    public PsiElement resolve()
    {
        ResolveResult[] resolveResults = multiResolve(false);
        return resolveResults.length == 1 ? resolveResults[0].getElement() : null;
    }

    @NotNull
    @Override
    public Object[] getVariants()
    {
        return new Object[0];
    }

    @Override
    public boolean isSoft() {
        return true;
    }
}
