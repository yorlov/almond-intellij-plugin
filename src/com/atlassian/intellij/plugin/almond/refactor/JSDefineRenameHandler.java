package com.atlassian.intellij.plugin.almond.refactor;

import com.intellij.openapi.actionSystem.CommonDataKeys;
import com.intellij.openapi.actionSystem.DataContext;
import com.intellij.openapi.editor.Editor;
import com.intellij.openapi.project.Project;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiFile;
import com.intellij.psi.PsiReference;
import com.intellij.refactoring.actions.BaseRefactoringAction;
import com.intellij.refactoring.rename.RenameHandler;

public class JSDefineRenameHandler implements RenameHandler {


    @Override
    public boolean isAvailableOnDataContext(DataContext dataContext) {
        Editor editor = CommonDataKeys.EDITOR.getData(dataContext);
        PsiFile psiFile = CommonDataKeys.PSI_FILE.getData(dataContext);
        if (editor != null && psiFile != null) {
            PsiElement psiElement = BaseRefactoringAction.getElementAtCaret(editor, psiFile);
            return psiElement != null;
        }
        return false;
    }

    @Override
    public boolean isRenaming(DataContext dataContext) {
        return isAvailableOnDataContext(dataContext);
    }

    @Override
    public void invoke(Project project, Editor editor, PsiFile psiFile, DataContext dataContext) {
        PsiElement psiElement = BaseRefactoringAction.getElementAtCaret(editor, psiFile);
        PsiReference reference = psiFile.findReferenceAt(editor.getCaretModel().getOffset());
        assert reference != null;
        reference.handleElementRename(psiElement.getText());
    }

    @Override
    public void invoke(Project project, PsiElement[] psiElements, DataContext dataContext) {
        return;
    }
}
