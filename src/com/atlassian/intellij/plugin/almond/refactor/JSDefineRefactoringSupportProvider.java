package com.atlassian.intellij.plugin.almond.refactor;

import com.intellij.lang.javascript.psi.JSLiteralExpression;
import com.intellij.lang.javascript.refactoring.JavascriptRefactoringSupportProvider;
import com.intellij.psi.PsiElement;
import org.jetbrains.annotations.NotNull;

public class JSDefineRefactoringSupportProvider extends JavascriptRefactoringSupportProvider
{
    @Override
    public boolean isMemberInplaceRenameAvailable(@NotNull PsiElement element, PsiElement context)
    {
        return element instanceof JSLiteralExpression;
    }

}
