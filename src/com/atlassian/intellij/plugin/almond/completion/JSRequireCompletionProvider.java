package com.atlassian.intellij.plugin.almond.completion;

import com.atlassian.intellij.plugin.almond.model.JSIndexSearchResult;
import com.atlassian.intellij.plugin.almond.utils.JSIndexSearcher;
import com.google.common.base.Function;
import com.google.common.collect.Iterables;
import com.intellij.codeInsight.completion.CompletionParameters;
import com.intellij.codeInsight.completion.CompletionProvider;
import com.intellij.codeInsight.completion.CompletionResultSet;
import com.intellij.codeInsight.lookup.AutoCompletionPolicy;
import com.intellij.codeInsight.lookup.LookupElement;
import com.intellij.codeInsight.lookup.LookupElementBuilder;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.util.text.StringUtil;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiFile;
import com.intellij.util.ProcessingContext;
import org.apache.commons.lang.StringUtils;
import org.jetbrains.annotations.NotNull;

import java.util.Set;

import static com.atlassian.intellij.plugin.almond.utils.ElementPatterns.isDefineCallExpression;
import static com.atlassian.intellij.plugin.almond.utils.ElementPatterns.isRequireCallExpression;

public class JSRequireCompletionProvider extends CompletionProvider<CompletionParameters>
{

    @Override
    protected void addCompletions(CompletionParameters completionParameters, ProcessingContext processingContext, final CompletionResultSet completionResultSet)
    {
        PsiElement currentElement = completionParameters.getOriginalPosition();
        PsiElement literalExpression = currentElement.getParent();

        if (!isRequireCallExpression(literalExpression) && !isDefineCallExpression(literalExpression))
        {
            return;
        }

        PsiFile origFile = completionParameters.getOriginalFile();
        Project currentProject = origFile.getProject();

        String searchingText = StringUtil.unquoteString(literalExpression.getText());

        Set<JSIndexSearchResult> searchResult = (StringUtils.isNotBlank(searchingText))
            ? JSIndexSearcher.find(searchingText, currentProject)
            : JSIndexSearcher.list(currentProject);
        Iterable<LookupElement> result = Iterables.transform(searchResult, new Function<JSIndexSearchResult, LookupElement>()
        {
            @Override
            public LookupElement apply(JSIndexSearchResult jsIndexSearchResult)
            {
                return buildLookupElementWithPath(jsIndexSearchResult.getKey(), jsIndexSearchResult.getVirtualFile().getName());
            }
        });
        completionResultSet.addAllElements(result);
    }

    public static
    @NotNull
    LookupElement buildLookupElementWithPath(@NotNull String path, String filePath)
    {
        return LookupElementBuilder
                .create(path)
                .withBoldness(true)
                .withPresentableText(path)
                .withCaseSensitivity(true)
                .withTypeText(filePath, true)
                .withAutoCompletionPolicy(AutoCompletionPolicy.GIVE_CHANCE_TO_OVERWRITE);
    }

}
