package com.atlassian.intellij.plugin.almond.completion;

import com.atlassian.intellij.plugin.almond.utils.ElementPatterns;
import com.intellij.codeInsight.completion.CompletionContributor;
import com.intellij.codeInsight.completion.CompletionType;

public class JSRequireCompletionContributor extends CompletionContributor
{

    public JSRequireCompletionContributor() {

        super();

        JSRequireCompletionProvider completionProvider = new JSRequireCompletionProvider();

        extend(CompletionType.BASIC, ElementPatterns.requirePattern, completionProvider);
        extend(CompletionType.BASIC, ElementPatterns.definePattern, completionProvider);
    }
}
